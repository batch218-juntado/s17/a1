/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//first function here:
	function printUserInformation(){
			let userFirstName=prompt("Enter your first name: ");
			let userLastName=prompt("Enter your last name: ");
			let userAge=prompt("Enter your age: ");
			let userLocation=prompt("Enter your location: ");

			console.log("Hello, " + userFirstName + " " + userLastName);
			console.log("You are " + userAge + " years old.");
			console.log("You live in " + userLocation);
		}

		printUserInformation();
	
	

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayUserFaveMusic(){
			console.log("1. Rammstein");
			console.log("2. Deftones");
			console.log("3. Siouxsie and the Banshees");
			console.log("4. Megumi Acorda");
			console.log("5. Bauhaus");
		}

		displayUserFaveMusic();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayUserFaveMovies(){
			console.log("1. Atonement");
			console.log("Rotten Tomatoes Rating: 83%");
			console.log("2. Pride and Prejudice (2005)");
			console.log("Rotten Tomatoes Rating: 87%");
			console.log("3. The Tale of Princess Kaguya");
			console.log("Rotten Tomatoes Rating: 100%");
			console.log("4. The Matrix (1999)");
			console.log("Rotten Tomatoes Rating: 88%");
			console.log("5. Crimson Peak");
			console.log("Rotten Tomatoes Rating: 73%");
		}

		displayUserFaveMovies();


/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function showPromptAlert(){
	alert("Hi! Please add the names of your friends.");
}

showPromptAlert();

printFriends();
function printFriends(){
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");


	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

	// console.log(friend1); 
	// console.log(friend2); 
	// console.log(friend3); 
